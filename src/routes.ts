import * as controller from './controller';

import express, { Request, Response } from 'express';
import { body, query, validationResult } from 'express-validator';

const router = express.Router();
export default router;

router.get('/', (req: Request, res: Response) => controller.home(req, res));

router.get('/toggleTheme', (req: Request, res: Response) => controller.toggleTheme(req, res));

router.get('/orderBy', [query('value').isIn(['dueDate', 'creationDate', 'importance'])], (req: Request, res: Response) => {
    validationResult(req).throw();
    const orderBy = <UserSettings['noteOrderBy']>req.query.value;
    controller.orderBy(req, res, orderBy);
});

router.get('/toggleFinishedNoteVisibility', (req: Request, res: Response) => controller.toggleFinishedNoteVisibility(req, res));

router.get('/createNote', (req: Request, res: Response) => controller.createNote(req, res));

router.get('/modifyNote', [query('uuid').isUUID(4)], (req: Request, res: Response) => {
    validationResult(req).throw();
    controller.modifyNote(req, res, <string>req.query.uuid);
});

router.get('/toggleNoteIsFinished', [query('uuid').isUUID(4)], (req: Request, res: Response) => {
    validationResult(req).throw();
    controller.toggleNoteIsFinished(req, res, <string>req.query.uuid);
});

router.post('/takeNote', [
    body('title').trim().isLength({ min: 1, max: 30 }),
    body('description').isLength({ max: 500 }),
    body('dueDate').isDate(),
    body('importance').isInt({ min: 1, max: 5 }).toInt(),
    body('isNewNote').isBoolean().toBoolean(),
    body('uuid').optional().isUUID(4)
], (req: Request, res: Response) => {
    validationResult(req).throw();
    controller.takeNote(req, res);
});
