declare interface UserSettings {
    theme: 'light' | 'dark';
    noteOrderBy: 'dueDate' | 'creationDate' | 'importance'
    noteOrder: 'ascending' | 'descending'
    visibleNotes: 'all' | 'unfinished'
}

declare namespace Express {
    export interface Request {
        userSettings: UserSettings
    }

    export interface Session {
        userSettings: UserSettings
    }
}