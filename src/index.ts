import express, { NextFunction, Request, Response } from 'express';
import session from 'express-session';
import crypto from 'crypto';
import handlebars from 'express-handlebars';
import Nedb from 'nedb';

import { userSettings } from './model/userSettings';
import { NoteStore } from './model/noteStore';
import { validationResult } from 'express-validator';

import routes from './routes'
import * as path from 'path';

const port = 8000;
const app = express();

const nedb = new Nedb({ filename: 'nedb.db', autoload: true });
export const noteStore = new NoteStore(nedb);

app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: crypto.randomBytes(64).toString('hex')
}));

app.use(express.urlencoded({ extended: true }));
app.use(userSettings);
app.use('/', routes);
app.use(express.static(path.resolve('public')));

const hbs = handlebars.create({
    helpers: {
        /* eslint @typescript-eslint/no-explicit-any: off */
        ifEq: function (left: any, right: any, options: any) {
            if (left === right) return options.fn(this);
            else return options.inverse(this);
        },
        loop: function (count: number, options: any) {
            let newContent = '';
            for (let i = 0; i < count; i++) {
                this.current = i;
                this.next = i + 1;
                newContent += options.fn(this);
            }
            return newContent;
        }
    }
});

app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

//Error handler
app.use((err: Error, req: Request, res: Response, _next: NextFunction) => {
    if (!validationResult(req).isEmpty()) {
        const message = validationResult(req).array().map(e => `Invalid value for parameter '${e.param}': ${e.msg}`);
        return res.status(400).send(message);
    }

    console.error(err.stack);
    res.status(500).send(err.message);
});

app.listen(port, () => {
    console.log(`Listening on http://localhost:${port}`)
});

process.on('SIGINT', () => {
    noteStore.persistNotes().catch(err => console.error(err)).finally(() => process.exit());
});
