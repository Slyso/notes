import { Note } from './note';
import Nedb from 'nedb';

export class NoteStore {
    private notes: Note[] = [];
    private store: Nedb;

    constructor(store: Nedb) {
        this.store = store;

        store.find({}, (err: Error | null, notes: Note[]) => {
            notes.forEach(note => this.addNote(Note.from(note)));
        });
    }

    addNote(note: Note): void {
        this.notes.push(note);
    }

    getNote(uuid: string): Note | undefined {
        return this.notes.find(note => note.uuid === uuid);
    }

    getNotes(userSettings: UserSettings | null): Note[] {
        if (!userSettings) return this.notes;

        let userSpecificNotes = this.notes;
        const descending = userSettings.noteOrder === 'descending';

        if (userSettings.visibleNotes === 'unfinished') {
            userSpecificNotes = userSpecificNotes.filter(note => !note.isFinished);
        }

        if (userSettings.noteOrderBy === 'creationDate') {
            userSpecificNotes.sort((a, b) => {
                if (descending) return a.creationDate.getTime() - b.creationDate.getTime();
                else return b.creationDate.getTime() - a.creationDate.getTime();
            });
        } else if (userSettings.noteOrderBy === 'dueDate') {
            userSpecificNotes.sort((a, b) => {
                if (descending) return a.dueDate.getTime() - b.dueDate.getTime();
                else return b.dueDate.getTime() - a.dueDate.getTime();
            });
        } else if (userSettings.noteOrderBy === 'importance') {
            userSpecificNotes.sort((a, b) => {
                if (descending) return b.importance - a.importance;
                else return a.importance - b.importance;
            });
        }

        return userSpecificNotes;
    }

    persistNotes(): Promise<void> {
        const noteAlreadyStored = (note: Note) => new Promise<boolean>((resolve, reject) => {
            this.store.count({ _uuid: note.uuid }, (error: Error | null, count: number) => {
                if (error) return reject(error);
                resolve(count > 0);
            });
        });

        const jobs: Promise<void>[] = [];

        this.notes.forEach(note => {
            jobs.push(new Promise((resolve, reject) => {
                noteAlreadyStored(note).then(isStored => {
                    if (isStored) {
                        this.store.update({ _uuid: note.uuid }, note, {}, (error) => {
                            if (error) reject(error);
                            else resolve();
                        });
                    } else {
                        this.store.insert(note, (error) => {
                            if (error) reject(error);
                            else resolve();
                        });
                    }
                })
            }));
        });

        return new Promise((resolve, reject) => {
            Promise.all(jobs).then(() => resolve()).catch(error => reject(error));
        });
    }
}

