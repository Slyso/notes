import { v4 as uuid } from 'uuid';

export class Note {
    public title: string;
    public description: string;
    public dueDate: Date;
    public importance: number;
    public isFinished = false;

    private _creationDate: Date = new Date(Date.now());
    private _uuid: string = uuid();

    constructor(title: string, description: string, dueDate: Date, importance: number) {
        this.title = title;
        this.description = description;
        this.dueDate = dueDate;
        this.importance = importance;
    }

    get creationDate(): Date {
        return this._creationDate;
    }

    get uuid(): string {
        return this._uuid;
    }

    get timeRemaining(): string {
        const today = new Date(new Date().toDateString());
        const dueDay = new Date(this.dueDate.toDateString());

        const milliseconds = dueDay.getTime() - today.getTime();
        if (milliseconds < 0) return 'Due!';

        const days = Math.floor(milliseconds / (1000 * 60 * 60 * 24));

        if (days == 0) return 'Today';
        if (days == 1) return 'Tomorrow';
        if (days < 30) return `In ${days} days`

        const months = Math.floor(days / 30);
        if (months == 1) return 'In one month';
        if (months < 12) return `In ${months} months`;

        const years = Math.floor(months / 12);
        if (years == 1) return 'Next year';

        return `In ${years} years`
    }

    get handlebarObject(): Record<string, unknown> {
        return {
            title: this.title,
            description: this.description,
            timeRemaining: this.timeRemaining,
            importance: this.importance,
            isFinished: this.isFinished,
            uuid: this.uuid
        };
    }

    static from(json: unknown): Note {
        return Object.assign(new Note('', '', new Date(), 1), json);
    }
}