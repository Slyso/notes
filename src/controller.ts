import { Request, Response } from 'express';

import * as settings from './model/userSettings';
import { Note } from './model/note';
import { noteStore } from './index';

export function home(req: Request, res: Response): void {
    res.render('home', {
        theme: req.userSettings.theme,
        showFinishedNotes: req.userSettings.visibleNotes === 'all',
        orderBy: req.userSettings.noteOrderBy,
        notes: noteStore.getNotes(req.userSettings).map(note => note.handlebarObject)
    });
}

export function toggleTheme(req: Request, res: Response): void {
    settings.toggleTheme(req.userSettings);
    res.redirect('/');
}

export function orderBy(req: Request, res: Response, orderBy: UserSettings['noteOrderBy']): void {
    if (orderBy === req.userSettings.noteOrderBy) settings.toggleNoteOrder(req.userSettings);
    else req.userSettings.noteOrderBy = orderBy;
    res.redirect('/');
}

export function toggleFinishedNoteVisibility(req: Request, res: Response): void {
    if (req.userSettings.visibleNotes === 'all') req.userSettings.visibleNotes = 'unfinished';
    else if (req.userSettings.visibleNotes === 'unfinished') req.userSettings.visibleNotes = 'all';
    res.redirect('/');
}

export function createNote(req: Request, res: Response): void {
    res.render('noteEditor', {
        isNewNote: true,
        importance: 1,
        theme: req.userSettings.theme,
    });
}

export function modifyNote(req: Request, res: Response, uuid: string): void {
    const note = noteStore.getNote(uuid);
    if (!note) throw new Error('Note not found');

    res.render('noteEditor', {
        isNewNote: false,
        theme: req.userSettings.theme,
        uuid: uuid,
        title: note.title,
        description: note.description,
        dueDate: note.dueDate.toISOString().split('T')[0],
        importance: note.importance
    });
}

export function toggleNoteIsFinished(req: Request, res: Response, uuid: string): void {
    const note = noteStore.getNote(uuid);
    if (!note) throw new Error('Note not found');
    note.isFinished = !note.isFinished;
    res.redirect('/');
}

export function takeNote(req: Request, res: Response): void {
    const title = req.body.title;
    const description = req.body.description;
    const dueDate = new Date(req.body.dueDate);
    const importance = req.body.importance;

    if (req.body.isNewNote) {
        const note = new Note(title, description, dueDate, importance);
        noteStore.addNote(note);
    } else {
        const uuid: string | undefined = req.body.uuid;
        if (!uuid) throw new Error('No UUID provided');

        const note = noteStore.getNote(uuid)
        if (!note) throw new Error('Note not found');

        note.title = title;
        note.description = description;
        note.dueDate = dueDate;
        note.importance = importance;
    }

    res.redirect('/');
}
